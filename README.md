[![GitHub issues](https://img.shields.io/github/issues/mfyo-biz/info.svg?style=plastic)](https://github.com/mfyo-biz/info/issues)
[![GitHub forks](https://img.shields.io/github/forks/mfyo-biz/info.svg?style=plastic)](https://github.com/mfyo-biz/info/network)
[![GitHub license](https://img.shields.io/github/license/mfyo-biz/info.svg?style=plastic)](https://github.com/mfyo-biz/info/blob/master/LICENSE)
    

# ぜにいんふぉ (開発コード : Cherry blossom) (ver1.5.0~)
ぜにいんふぉは暗号資産BitZenyの情報をまとめて表示するWebアプリです。  
このリポジトリはver.1.5.0からのソースコードを掲載しています。  
[ver.1.3.1までのソースコード](https://github.com/mfyo-biz/zny-info)  
  
# 開発方針  
  
# 開発言語  
PHP 7.2.18    
Uikit  
Vine.js  
# 推奨ブラウザ  
Firefox、Chromeなどのモダンブラウザの最新版  
(Internet Explorerなどのレガシーブラウザでは利用できません)  
# ライセンス  
MIT License  
  
  
# 作者  
mfyo.biz (Yocchan1513 Project)  
